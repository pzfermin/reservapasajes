package pasajero2;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class CheckIdioma extends JFrame implements ChangeListener{
    
     JCheckBox ingles, castellano, aleman, italiano, portugues, frances;
    
    public CheckIdioma(){
        ingles = new JCheckBox("Ingles");
        ingles.addChangeListener(this);
        castellano = new JCheckBox("Castellano");
        castellano.addChangeListener(this);
        aleman = new JCheckBox("Aleman");
        aleman.addChangeListener(this);
        italiano = new JCheckBox("Italiano");
        italiano.addChangeListener(this);
        portugues = new JCheckBox("Portugues");
        portugues.addChangeListener(this);
        frances = new JCheckBox("Frances");
        frances.addChangeListener(this);
        
    }
    
    @Override
    public void stateChanged(ChangeEvent e){
        String cad = "";
        if (ingles.isSelected()==true)
        {
            cad="Ingles";
        }
        if (castellano.isSelected()==true){
            cad="Castellano";
        }
        if (aleman.isSelected()==true){
            cad="Aleman";
        }
        if (italiano.isSelected()==true){
            cad="Italiano";
        }
        if (portugues.isSelected()==true){
            cad="Portugues";
        }
        if (frances.isSelected()==true) {
            cad="Frances";
        }
    }
    
    
}
