package pasajero2;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Estado extends JFrame implements ChangeListener{
    
    JRadioButton soltero, casado, separado, viudo, comprometido;
    ButtonGroup bg;
    String cadena;
    public Estado(){
        bg = new ButtonGroup();
        bg.add(soltero);
        bg.add(casado);
        bg.add(separado);
        bg.add(comprometido);
        bg.add(viudo);
        soltero = new JRadioButton("Soltero");
        soltero.addChangeListener(this);
        casado = new JRadioButton("Casado");
        casado.addChangeListener(this);
        separado = new JRadioButton("Separado");
        separado.addChangeListener(this);
        viudo = new JRadioButton("Viudo");
        viudo.addChangeListener(this);
        comprometido = new JRadioButton("Comprometido");
        comprometido.addChangeListener(this);
        
    }
    
    @Override
    public void stateChanged (ChangeEvent event){
        
        if(soltero.isSelected()==true){
            cadena="Soltero";
            
        }
        else if (casado.isSelected()==true){
            cadena="Casado";
            
        }
        else if (separado.isSelected()==true){
            cadena="Separado";
            
        }
        else if (viudo.isSelected()==true){
            cadena="Viudo";
            
        }
        else if (comprometido.isSelected()==true){
            cadena="Comprometido";
            
        }
        
        
    }
    
    
}
