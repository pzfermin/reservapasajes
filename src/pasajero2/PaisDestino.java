package pasajero2;

import javax.swing.*;


public class PaisDestino extends JFrame {
    
    final JComboBox Lista;
    
    public PaisDestino(){
        String paises [] = {"Inglaterra", "Portugal", "Argentina", "España", "Italia", "Francia"};
        Lista = new JComboBox(paises);
    }
    
    public String comboSelected(){
        String pais = Lista.getSelectedItem().toString();
        return pais;
    }
}
