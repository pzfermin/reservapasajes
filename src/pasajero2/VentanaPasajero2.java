package pasajero2;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;



public class VentanaPasajero2 extends JFrame implements ActionListener {
    
    private JLabel nombre, apellido, estadoCivil, paisTexto, idioma;
    private JTextField campoNombre, campoApellido;
    private JButton aceptar, salir;
    private JRadioButton estado;
    
    public VentanaPasajero2(){
        
        this.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints punto = new GridBagConstraints();
        
        nombre = new JLabel ("Nombre");
            punto.gridx = 0; // Columna 0. No necesita estirarse, no ponemos weightx
            punto.gridy = 0; // Fila 0. Necesita estirarse, hay que poner weighty
            punto.gridwidth = 1;
            punto.gridheight = 1; 
            punto.weighty = 1.0;
            punto.weightx = 1.0;
            this.getContentPane().add (nombre, punto); 
            
        
        campoNombre = new JTextField(10);
        add(campoNombre);
            punto.gridx = 1;
            punto.gridy = 0;
            punto.gridwidth = 1;
            punto.gridheight = 1;
            punto.weightx = 1;
            this.getContentPane().add(campoNombre , punto);
        
        
        apellido = new JLabel("Apellido");
            punto.gridx=2;
            punto.gridy=0;
            punto.gridwidth=1;
            punto.gridheight=1;
            punto.weightx=1;
            this.getContentPane().add(apellido, punto);
        
        campoApellido = new JTextField(10);
            punto.gridx=3;
            punto.gridy=0;
            punto.gridwidth=1;
            punto.gridheight=1;
            punto.weightx=1;
            this.getContentPane().add(campoApellido, punto);
        
        estadoCivil = new JLabel("Estado Civil");
            punto.gridx=0;
            punto.gridy=1;
            punto.gridwidth=1;
            punto.gridheight=1;
          
            this.getContentPane().add(estadoCivil, punto);
            
            
        
        Estado estado = new Estado();
            punto.gridx=0;
            punto.gridy=2;
            punto.weightx=1;
            punto.weighty=1;
            this.getContentPane().add(estado.casado, punto);
            
            punto.gridx=1;
            punto.gridy=2;
            this.getContentPane().add(estado.comprometido, punto);
            
            punto.gridx=2;
            punto.gridy=2;
            this.getContentPane().add(estado.separado, punto);
            
            punto.gridx=3;
            punto.gridy=2;
            this.getContentPane().add(estado.soltero, punto);
            
            punto.gridx=4;
            punto.gridy=2;
            this.getContentPane().add(estado.viudo, punto);

            idioma = new JLabel("Tilde los idiomas que habla");
            punto.gridx=0;
            punto.gridy=3;
            
            this.getContentPane().add(idioma, punto);

            CheckIdioma checkIdioma = new CheckIdioma();
            
            punto.gridx=0;
            punto.gridy=4;                             
            this.getContentPane().add(checkIdioma.ingles, punto);
            
            punto.gridx=1;
            punto.gridy=4;
            this.getContentPane().add(checkIdioma.aleman, punto);
            
            punto.gridx=2;
            punto.gridy=4;
            this.getContentPane().add(checkIdioma.castellano, punto);
            
            punto.gridx=3;
            punto.gridy=4;
            this.getContentPane().add(checkIdioma.portugues,punto);
            
            punto.gridx=4;
            punto.gridy=4;
            this.getContentPane().add(checkIdioma.italiano,punto);
            
            punto.gridx=0;
            punto.gridy=5;
            this.getContentPane().add(checkIdioma.frances, punto);
            

            paisTexto = new JLabel("Pais de destino");
            punto.gridx=0;
            punto.gridy=6;
            this.getContentPane().add(paisTexto,punto);

            
            PaisDestino paisDestino = new PaisDestino();
            punto.gridx=1;
            punto.gridy=6;
            this.getContentPane().add(paisDestino.Lista,punto);
            
            aceptar = new JButton("Aceptar");
            aceptar.addActionListener(this);
            punto.gridx=3;
            punto.gridy=6;
            this.getContentPane().add(aceptar, punto);
            
            salir = new JButton("Salir");
            salir.addActionListener(this); 
            punto.gridx=4;
            punto.gridy=6;
            this.getContentPane().add(salir, punto);
                    
    }
    @Override
        public void actionPerformed(ActionEvent e){
            if (e.getSource()==aceptar){
                JOptionPane.showMessageDialog(null,"Nombre: " + campoNombre.getText() + "\n" + "Apellido: "
                        + campoApellido.getText() + "\n" + "Estado civil: "//aca iria los datos que faltan obtener  
                );
            }
            else{
                System.exit(0);
            }
        }
}
